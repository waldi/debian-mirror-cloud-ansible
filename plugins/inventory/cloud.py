from __future__ import (absolute_import, division, print_function)

DOCUMENTATION = '''
    name: cloud
    plugin_type: inventory
    short_description: libcloud inventory source
    description:
        - Get inventory hosts from clouds supported by libcloud.
        - Uses cloud.(yml|yaml) and *.cloud.(yml|yaml) YAML configuration file to configure the inventory plugin.
        - Uses matching cloud.cred.(yml|yaml) and *.cloud.cred.(yml|yaml) YAML configuration file to configure cloud credentials.
'''

import os
import re

from libcloud.compute.types import Provider
from libcloud.compute.providers import get_driver

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

from ansible.errors import AnsibleParserError, AnsibleFileNotFound
from ansible.plugins.inventory import BaseInventoryPlugin
from ansible.template import Templar


class InventoryProvider(object):
    def __init__(self, loader, info, creds):
        self.loader = loader
        self.info = info
        self.creds = creds

    def get_inventory_hostname(self, node):
        inventory_hostname = self.info.get('inventory_hostname', node.name)
        templar = Templar(loader=self.loader)
        templar.set_available_variables({'node': node})
        return templar.template(inventory_hostname)

    def set_labels(self, inventory, name, labels):
        out = {}
        for k, v in labels.items():
            k = k.lower()
            out[k] = v
            label_group = 'label_{}={}'.format(k, v)
            inventory.add_group(label_group)
            inventory.add_host(name, label_group)
        inventory.set_variable(name, 'labels', out)


class InventoryProviderEC2(InventoryProvider):
    def __init__(self, loader, info, creds):
        super(InventoryProviderEC2, self).__init__(loader, info, creds)

        region = info['region']

        display.debug('Loading EC2 provider for region {}'.format(region))

        self.driver = get_driver(Provider.EC2)(region=region, **creds)

    def fill_inventory(self, inventory):
        inventory.add_group('ec2')
        region_group = 'ec2_region_{}'.format(self.driver.region_name)
        inventory.add_group(region_group)
        inventory.add_child('ec2', region_group)
        inventory.set_variable(region_group, 'ec2_region', self.driver.region_name)

        for zone in self.driver.ex_list_availability_zones():
            zone_group = 'ec2_zone_{}'.format(zone.name)
            inventory.add_group(zone_group)
            inventory.add_child(region_group, zone_group)
            inventory.set_variable(zone_group, 'ec2_zone', zone.name)

        for node in self.list_nodes():
            name = self.get_inventory_hostname(node)

            inventory.add_host(name, 'ec2_zone_{}'.format(node.extra['availability']))

            inventory.set_variable(name, 'ansible_host', node.extra['dns_name'])
            self.set_labels(inventory, name, node.extra['tags'])

    def list_nodes(self):
        return self.driver.list_nodes(ex_filters=self.info.get('instance_filters', {}))


class InventoryProviderGCE(InventoryProvider):
    def __init__(self, loader, path, info, creds):
        super(InventoryProviderGCE, self).__init__(loader, info, creds)

        self.project_id = info['project_id']

        creds_file = creds.get('gce_service_account_creds_file', info.get('gce_service_account_creds_file'))

        if not creds_file:
            raise AnsibleParserError()

        secrets = loader.load_from_file(os.path.join(path, creds_file))

        display.debug('Loading GCE provider using creds from {}'.format(creds_file))

        self.driver = get_driver(Provider.GCE)(
            user_id=secrets['client_email'],
            key=secrets['private_key'],
            project=self.project_id,
        )

        def patch_to_node(node, **kw):
            r = self.driver.__class__._to_node(self.driver, node, **kw)
            r.extra['labels'] = node.get('labels', {})
            return r
        self.driver._to_node = patch_to_node

    def fill_inventory(self, inventory):
        inventory.add_group('gce')

        for region in self.driver.region_list:
            region_group = 'gce_region_{}'.format(region.name)
            inventory.add_group(region_group)
            inventory.add_child('gce', region_group)
            inventory.set_variable(region_group, 'gce_region', region.name)
            for zone in region.zones:
                zone_group = 'gce_zone_{}'.format(zone.name)
                inventory.add_group(zone_group)
                inventory.add_child(region_group, zone_group)
                inventory.set_variable(region_group, 'gce_zone', zone.name)

        for node in self.list_nodes():
            name = self.get_inventory_hostname(node)

            inventory.add_host(name, 'gce_zone_{}'.format(node.extra['zone'].name))

            inventory.set_variable(name, 'ansible_host',
                    node.public_ips[0] if len(node.public_ips) >= 1 else node.private_ips[0])
            inventory.set_variable(name, 'gce_name', node.name)
            inventory.set_variable(name, 'gce_project_id', self.project_id)
            self.set_labels(inventory, name, node.extra['labels'])

    def list_nodes(self):
        nodes = self.driver.list_nodes()

        for node in nodes:
            if self._check_instance_filter(node):
                yield node

    def _check_instance_filter(self, node):
        instance_filters = self.info.get('instance_filters', {})

        if instance_filters:
            for f_info, f_value in instance_filters.items():
                if ':' in f_info:
                    f_info, f_var = f_info.split(':', 1)
                if f_info == 'label':
                    l = node.extra['labels'].get(f_var)
                    if f_value != l:
                        return False
                else:
                    raise AnsibleParserError('Unsupported instance filter: {}={}'.format(f_info, f_value))

        return True



class InventoryModule(BaseInventoryPlugin):
    NAME = 'cloud'

    def verify_file(self, path):
        if (super(InventoryModule, self).verify_file(path) and
                re.search('[/.]cloud\.(creds\.)?ya?ml$', path)):
            return True

    def parse(self, inventory, loader, path, cache=True):
        super(InventoryModule, self).parse(inventory, loader, path)

        if not self._load_config(path):
            return False

        for name, info in sorted(self._config_data.iteritems()):
            if name.startswith("."):
                continue

            creds = self._config_creds.get(name, {})

            provider = self._load_provider(path, info, creds)
            provider.fill_inventory(inventory)

    def _load_config(self, path):
        if not re.search('[/.]cloud\.ya?ml$', path):
            return False

        try:
            self._config_data = self.loader.load_from_file(path)
        except Exception as e:
            raise AnsibleParserError(e)

        try:
            f = re.sub(r'([/.]cloud)\.(ya?ml)$', r'\1.creds.\2', path)
            self._config_creds = self.loader.load_from_file(f)
        except AnsibleFileNotFound:
            self._config_creds = {}
        except Exception as e:
            raise AnsibleParserError(e)

        return True

    def _load_provider(self, path, info, creds):
        provider_type = info['provider']
        if provider_type == 'EC2':
            return InventoryProviderEC2(self.loader, info, creds)
        elif provider_type == 'GCE':
            return InventoryProviderGCE(self.loader, os.path.dirname(path), info, creds)
        else:
            raise AnsibleParserError('Unknown provider: ' + provider_type)


if __name__ == '__main__':
    import sys
    from pprint import pprint

    from ansible.inventory.data import InventoryData
    from ansible.parsing.dataloader import DataLoader

    loader = DataLoader()
    inventory = InventoryData()
    module = InventoryModule()
    module.parse(inventory, loader, sys.argv[1], False)

    for name, i in sorted(inventory.hosts.items()):
        pprint(i.__dict__)
